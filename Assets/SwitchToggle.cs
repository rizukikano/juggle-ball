﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SwitchToggle : MonoBehaviour
{
    public Toggle panelToggle;
    public GameObject ImageOn;
    public GameObject ImageOff;
    public AudioSource sound;
    // Start is called before the first frame update
    void Start()
    {
        panelToggle.onValueChanged.AddListener((value) =>
        {
            Panel(value);
        });
    }


    public void Panel(bool isOn)
    {

        if (isOn)
        {
            ImageOn.SetActive(true);
            ImageOff.SetActive(false);
            sound.mute= !sound.mute;

        }
        else
        {
            ImageOff.SetActive(true);
            ImageOn.SetActive(false);
            sound.mute = !sound.mute;

        }
        

    }

    

}
