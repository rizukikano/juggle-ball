﻿using UnityEngine;

public class ifLose : MonoBehaviour
{
    private static ifLose _instance;
    public static ifLose instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
    }

    public GameManager gameManager;
    public GameObject prefab;

    public void setLost(bool lost)
    {
        if (lost)
        {
            OnLose();
        }
    }

    private void OnLose()
    {
        GameObject ball = GameObject.FindObjectOfType<Ball>().gameObject;
        GameObject go = Instantiate(prefab, ball.GetComponent<Transform>().position, Quaternion.identity);

        if (gameManager.score > PlayerPrefs.GetInt("HighScore"))
        {
            PlayerPrefs.SetInt("HighScore", gameManager.score);
            if (Leaderboard.instance != null)
            {
                Leaderboard.instance.UpdateLeaderboard(gameManager.score);
            }
            else
            {
                Debug.Log("Leaderboard not found!");
            }
        }

        ball.SetActive(false);
        Destroy(go, 1f);
        gameManager.GameOver();
    }
}