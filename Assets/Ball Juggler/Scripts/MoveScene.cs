﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoveScene : MonoBehaviour
{
    private static MoveScene _instance;
    public static MoveScene instance { get { return _instance; } }
    private void Awake()
    {
        if(_instance == null)
        {
            _instance = this;
        }
        
        animator = GetComponent<Animator>();
        animator.Play("fadeIn");
    }

    Animator animator;
    string targetSceneName;
    public void startMoveScene(string sceneName)
    {
        targetSceneName = sceneName;
        StartCoroutine("waitfortransition");
    }

    IEnumerator waitfortransition()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        animator.Play("fadeOut");
        SceneManager.LoadScene(targetSceneName);
        Time.timeScale = 1;
    }
}