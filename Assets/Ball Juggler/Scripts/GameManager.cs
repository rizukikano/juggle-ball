﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Text scoreText;
    public int score;
    public GameObject resultPanel;
    public GameObject mainPanel;
    public Text resultText;
    public Text highScoreText;
    public float timeRemaining = 10;
    public bool timerIsRunning = false;
    public Text timeText;

    void Start()
    {
        score = 0;
        timerIsRunning = true;
    }

    void DisplayTime(float timeToDisplay)
    {
        timeToDisplay += 1;

        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        timeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    void Update()
    {
        scoreText.text = score.ToString();
        if (timerIsRunning)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
                DisplayTime(timeRemaining);
            }
            else
            {
                timeRemaining = 0;
                timerIsRunning = false;
                GameOver();
            }
        }
    }
    public void GameOver()
    {

        resultPanel.SetActive(true);
        resultText.text = score.ToString();
        highScoreText.text = PlayerPrefs.GetInt("HighScore").ToString();
        timerIsRunning = false;
        mainPanel.SetActive(false);
    }

}