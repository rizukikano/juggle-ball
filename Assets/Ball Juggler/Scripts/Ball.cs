﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    SFX sfx;
    Rigidbody ballRigibody;
    private float numberX;
    private int minX = -4;
    private int maxX = 4;
    private float numberY;
    private float power = 1;
    public GameManager gameManager;
    public ParticleSystem vfx;

    private void Start()
    {
        sfx = GameObject.FindGameObjectWithTag("SFX").GetComponent<SFX>();
    }
    void Awake()
    {
        ballRigibody = this.GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Player")
        {
            sfx.soundFX.Play();
            numberX = Random.Range(minX, maxX);
            numberY = Random.Range(4, 8);
            //power = Random.Range(2, 4);

            ballRigibody.velocity = new Vector3(0, 0, 0);
            ballRigibody.AddForce(new Vector3(numberX, numberY * power, 0), ForceMode.Impulse);
            power += 0.05f;
            gameManager.score += 1;
            if (gameManager.score % 10 == 0)
            {
                minX -= 2;
                maxX += 2;
                float red = Random.Range(0f, 1f);
                float green = Random.Range(0f, 1f);
                float blue = Random.Range(0f, 1f);
                Debug.Log(red + green + blue);
                vfx.startColor = new Color(red, green, blue, 1);
            }

        }
        else if (collision.collider.tag == "Ground")
        {
            ifLose.instance.setLost(true);
            sfx.loseFx.Play();
        }

        if (collision.collider.tag == "WallRight")
        {
            ballRigibody.AddForce(-(transform.right) * 5f, ForceMode.Impulse);
        }

        if (collision.collider.tag == "WallLeft")
        {
            ballRigibody.AddForce(transform.right * 5f, ForceMode.Impulse);
        }
    }
}