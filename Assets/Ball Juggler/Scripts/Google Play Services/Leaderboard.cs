﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Leaderboard : MonoBehaviour
{
    private static Leaderboard _instance;
    public static Leaderboard instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
    }

    public void openLeaderboard()
    {
        Social.ShowLeaderboardUI();
    }

    public void UpdateLeaderboard(int highscore)
    {
        Social.ReportScore(PlayerPrefs.GetInt("HighScore", highscore), GPGSIds.leaderboard_top_scorer,
            (bool success) =>
            {
                if (success)
                {
                    PlayerPrefs.SetInt("HighScore", highscore);
                }
            });
    }
}