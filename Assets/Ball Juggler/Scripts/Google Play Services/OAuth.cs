﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class OAuth : MonoBehaviour
{
    public static PlayGamesPlatform playGamesPlatform;
    void Start()
    {
        if (playGamesPlatform == null)
        {
            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
            PlayGamesPlatform.InitializeInstance(config);
            PlayGamesPlatform.DebugLogEnabled = true;

            playGamesPlatform = PlayGamesPlatform.Activate();
        }

        Social.Active.localUser.Authenticate(success =>
        {
            if (success)
            {
                Debug.Log("Login Successfull!");
            }
            else
            {
                Debug.Log("Login Failed!");
            }
        });
    }
}