﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class Achievement : MonoBehaviour
{
    private static Achievement _instance;
    public static Achievement instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
    }

    public void unlockAchievement(int n) //n achievement 
    {
        switch (n)
        {
            case 0:
                firstJugglingAchievement();
                break;
            case 1:
                firstFailJugglingAchievement();
                break;
            default:
                break;
        }
    }

    public void openAchievement()
    {
        Social.ShowAchievementsUI();
    }

    void firstFailJugglingAchievement()
    {

    }

    void firstJugglingAchievement()
    {
        Social.ReportProgress(GPGSIds.achievement_first_juggling, 100f, null); //achievement name, percentage, custom callback
    }
}