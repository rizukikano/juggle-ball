﻿using UnityEngine;
using UnityEngine.SceneManagement;
public class Pause : MonoBehaviour
{
    private static Pause _instance;
    public static Pause instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance == null)
        {       
            _instance = this;
        }
    }

    public GameObject panelPause;
    public bool isPaused;
    public void PauseControl()
    {
        if (Time.timeScale == 1)
        {
            isPaused = true;
            panelPause.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            isPaused = false;
            Time.timeScale = 1;
            panelPause.SetActive(false);
        }
    }
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1;
    }
}