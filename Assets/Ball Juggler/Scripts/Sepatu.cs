﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sepatu : MonoBehaviour
{
    [SerializeField] Transform jagangKanan;
    [SerializeField] Transform jagangKiri;
    void Update()
    {
        if (Input.GetMouseButton(0) || Input.touchCount > 0)
        {
            if (Pause.instance.isPaused == false)
            {
                playerControl();
            }
            else
            {
                transform.position = this.transform.position;
            }
        }
        else
        {
            transform.position = this.transform.position;
        }

        void playerControl()
        {
            Vector3 screenPos = Input.mousePosition;
            screenPos.z = -1 * Camera.main.transform.position.z;
            Vector3 worldPos = Camera.main.ScreenToWorldPoint(screenPos);
            Vector3 newPos = transform.position;
            newPos.x = worldPos.x;

            if (newPos.x >= jagangKanan.position.x)
            {
                newPos.x = jagangKanan.position.x - 1;
            }
            if (newPos.x <= jagangKiri.position.x)
            {
                newPos.x = jagangKiri.position.x + 1;
            }
            transform.position = newPos;
        }
    }
}
